<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $tags = [
        [
          'name' => 'adminRojak',
          'email' => 'adminRojak@gmo.com',
          'password' => bcrypt('PhParTisanMAke:AutH'),
          'remember_token' => ''
        ],
        [
          'name' => 'adminMaseko',
          'email' => 'adminMaseko@gmo.com',
          'password' => bcrypt('CHmodd777ADmin'),
          'remember_token' => ''
        ],
        [
          'name' => 'adminAwe',
          'email' => 'adminAwe@gmo.com',
          'password' => bcrypt('SUdoARtisan8000'),
          'remember_token' => ''
        ],
        [
          'name' => 'adminVirgi',
          'email' => 'adminVirgi@gmo.com',
          'password' => bcrypt('Cp-RGMoserVe'),
          'remember_token' => ''
        ]
      ];
      foreach ($tags as $tag) {
          User::create( $tag);
    }
}
}
