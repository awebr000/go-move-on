<?php

use Illuminate\Database\Seeder;
use App\Tag;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = ['highlight', 'berita_terbaru', 'hot_news'];
        foreach ($tags as $tag) {
            Tag::create(['name' => $tag]);
        }
    }
}
