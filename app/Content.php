<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    /** tag_id info
     * 1 = highlight
     * 2 = berita_terbaru
     * 3 = hot_news
     */

    use SoftDeletes;

    protected $fillable = [
        'title',
        'meta_title',
        'sm_thumb_image',
        'bg_thumb_image',
        'description',
        'tag_id',
        'content',
        'views',
        'published_at'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id');
    }

    public function plusView()
    {
        $this->views += 1;
        $this->save();
    }
}
