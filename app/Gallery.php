<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    /** TYPES
     * carousel => should hyperlinked to article
     * content_image => inside article
     * bg_content_thumbnail => article
     * sm_content_thumbnail => article
     * cube_image => latest (4 items)
     * galleries => latest (4 items)
     */
    protected $fillable = [
        'type', 'url', 'active'
    ];
}
