<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class SliderImagesController extends Controller
{
    public function index()
    {
        $activeImages = Gallery::where('type', 'image_sliders')->where('active', 1)->paginate(25);
        $inactiveImages = Gallery::where('type', 'image_sliders')->where('active', 0)->paginate(25);
        // dd($activeImages);
        return view('admin.sliders.index', compact('activeImages', 'inactiveImages'));
    }

    public function store(Request $request)
    {
        $file = $request->file('file');
        $name = $file->getClientOriginalName();
        $name = explode('.', $name);
        $name = time() . '_' . strtolower($name[0]) . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('uploads/sliders'), $name);
        $image = Gallery::create([
            'type' => $request->input('type'),
            'url' => url('uploads/sliders/'. $name)
        ]);

        return redirect()->route('admin.slider-images.index');
    }

    public function massUpdate(Request $request)
    {
        foreach ($request->input('ids') as $id) {
            $slider = Gallery::find($id);
            if ($request->has('activate')) {
                $slider->active = 1;
            } else {
                $slider->active = 0;
            }
            $slider->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'data updated successfully'
        ]);
    }
}
