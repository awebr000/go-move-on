<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = Content::all();
        $data['highlight'] = $this->getHighlight($contents);
        $data['berita_terbaru'] = $this->getNews($contents);
        $data['hot_news'] = $this->getHot($contents);

        return view('admin.index', compact('data'));
    }


    public function login()
    {


        return view('admin.Login_v1.Login_v1.index');
    }

    public function auth_login(Request $request){
      //dd($request->all());
      if (Auth::attempt(['email'=> $request->input('email'), 'password'=>$request->input('pass')])) {
        return redirect('admin/contents');
      } else {
        return "err";
      }

    }



    public function getHighlight($contents)
    {
        $total = 0;
        $published = 0;
        $unpublished = 0;
        foreach ($contents as $con) {
            if ($con->tag_id == 1) {
                $total += 1;
                if ($con->published_at) {
                    $published += 1;
                } else {
                    $unpublished += 1;
                }
            }
        }
        $data = [
            'total' => $total,
            'published' => $published,
            'unpublished' => $unpublished
        ];
        return $data;
    }

    public function getNews($contents)
    {
        $total = 0;
        $published = 0;
        $unpublished = 0;
        foreach ($contents as $con) {
            if ($con->tag_id == 2) {
                $total += 1;
                if ($con->published_at) {
                    $published += 1;
                } else {
                    $unpublished += 1;
                }
            }
        }
        $data = [
            'total' => $total,
            'published' => $published,
            'unpublished' => $unpublished
        ];
        return $data;
    }

    public function getHot($contents)
    {
        $total = 0;
        $published = 0;
        $unpublished = 0;
        foreach ($contents as $con) {
            if ($con->tag_id == 3) {
                $total += 1;
                if ($con->published_at) {
                    $published += 1;
                } else {
                    $unpublished += 1;
                }
            }
        }
        $data = [
            'total' => $total,
            'published' => $published,
            'unpublished' => $unpublished
        ];
        return $data;
    }
}
