<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Gallery;

class HomeController extends Controller
{
    public function index()
    {
        $highlight = Content::whereNotNull('published_at')->where('tag_id', 1)->orderBy('published_at', 'desc')->first();
        $berita_terbaru = Content::whereNotNull('published_at')->where('tag_id', 2)->orderBy('published_at', 'desc')->first();
        $hot_news = Content::whereNotNull('published_at')->where('tag_id', 3)->orderBy('published_at', 'desc')->first();
        $sliders = Gallery::where('type', 'image_sliders')->where('active', 1)->limit(3)->get();
        $galleries = Gallery::where('type', 'galleries')->limit(3)->get();

        return view('home.index', compact('highlight', 'berita_terbaru', 'hot_news', 'sliders', 'galleries'));
    }

    public function hotnewsIndex()
    {
        $contents = Content::where('tag_id', 3)->whereNotNull('published_at')->orderBy('published_at', 'desc')->paginate(15);
        $title = 'hotnews';
        return view('home.contents.index', compact('contents', 'title'));
    }

    public function beritaBaruIndex()
    {
        $contents = Content::where('tag_id', 2)->whereNotNull('published_at')->orderBy('published_at', 'desc')->paginate(15);
        $title = 'hotnews';
        return view('home.contents.index', compact('contents', 'title'));
    }

    public function highlightIndex()
    {
        $contents = Content::where('tag_id', 1)->whereNotNull('published_at')->orderBy('published_at', 'desc')->paginate(15);
        $title = 'hotnews';
        return view('home.contents.index', compact('contents', 'title'));
    }

    public function galleries()
    {
        $galleries = Gallery::where('type', 'galleries')->paginate(32);

        return view('home.galleries.index', compact('galleries'));
    }
}
