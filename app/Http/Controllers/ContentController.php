<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Content;
use App\Tag;
use Carbon\Carbon;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        $contents = Content::with('tag')->paginate(10);
        return view('admin.contents.index', compact('contents'));
    }

    public function show(Request $request, $content)
    {
        $showedContent = Content::where('meta_title', $content)->first();
        $title = $showedContent->title;
        return view('home.contents.show', compact('showedContent', 'title'));
    }

    public function create()
    {
        $tags = Tag::all();
        return view('admin.contents.create', compact('tags'));
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['meta_title'] = time() . '-' . str_replace(' ', '-', $request->title);
        if ($request->has('publish')) {
            $data['published_at'] = Carbon::now();
        }

        $content = Content::create($data);
        $content->meta_title = $content->created_at->format('dmYhis') . '-' . str_replace(' ', '-', $content->title);
        $content->save();
        return response()->json([
            'success' => 'true',
            'message' => 'article created successfully'
        ]);
    }

    public function edit($id)
    {
        $content = Content::find($id);
        $tags = Tag::all();
        return view('admin.contents.edit', compact('content', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $content = Content::find($id);
        $data = $request->except('_token');
        $meta_title = preg_replace('/[?]/', '', $request->title);
        $data['meta_title'] = $content->created_at->format('dmYhis') . '-' . str_replace(' ', '-', $meta_title);
        if ($request->has('publish')) {
            $data['published_at'] = Carbon::now();
        } elseif ($request->has('unpublish')) {
            $data['published_at'] = null;
        }

        $new_content = $content->update($data);

        return response()->json([
            'success' => 'true',
            'message' => 'article updated successfully'
        ]);
    }
}
