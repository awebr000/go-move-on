<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;

class GalleriesController extends Controller
{
    public function index()
    {
        $galleries = Gallery::where('type', 'galleries')->paginate(12);

        return view('admin.galleries.index', compact('galleries'));
    }
    
    public function upload(Request $request)
    {
        $file = $request->file('image');
        $name = $file->getClientOriginalName();
        $name = explode('.', $name);
        $name = time() . '_' . strtolower($name[0]) . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('uploads/contents'), $name);
        $image = Gallery::create([
            'type' => $request->input('type'),
            'url' => url('uploads/contents/'. $name)
        ]);
        return $image->url;
    }

    public function store(Request $request)
    {
        $file = $request->file('image');
        $name = $file->getClientOriginalName();
        $name = explode('.', $name);
        $name = time() . '_' . strtolower($name[0]) . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('uploads/galleries'), $name);
        $image = Gallery::create([
            'type' => $request->input('type'),
            'url' => url('uploads/galleries/'. $name)
        ]);
        return redirect()->route('admin.galleries.index');
    }
}
