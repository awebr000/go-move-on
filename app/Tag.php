<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name'];
    public function contents()
    {
        return $this->hasMany(Content::class, 'tag_id');
    }
}
