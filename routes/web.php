<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/hotnews', 'HomeController@hotnewsIndex')->name('home.hotnews.index');
Route::get('/berita-terbaru', 'HomeController@beritaBaruIndex')->name('home.beritaTerbaru.index');
Route::get('/highlight', 'HomeController@highlightIndex')->name('home.highlight.index');
Route::get('/galleries', 'HomeController@galleries')->name('home.galleries.index');
Route::get('/hotnews/{content}', 'ContentController@show')->name('content.hotnews.show');
Route::get('/berita-terbaru/{content}', 'ContentController@show')->name('content.beritaTerbaru.show');
Route::get('/highlight/{content}', 'ContentController@show')->name('content.highlight.show');
/* ADMIN PANEL */
Route::get('admin/login', 'AdminController@login')->name('login');
Route::post('admin/login','AdminController@auth_login');

Route::name('admin.')->prefix('admin')->middleware('auth')->group(function () {
    Route::get('/', 'AdminController@index');
    Route::resource('/contents', 'ContentController');
    Route::get('/galleries', 'GalleriesController@index')->name('galleries.index');
    Route::post('/galleries', 'GalleriesController@store')->name('galleries.store');
    Route::post('/images', 'GalleriesController@upload')->name('UploadImage');
    Route::resource('/slider-images', 'SliderImagesController');
    Route::put('/mass-update/slider-images', 'SliderImagesController@massUpdate')->name('slider-images.massUpdate');
});
