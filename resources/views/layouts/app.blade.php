<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>GMO Event Media @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/base.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/cube.css') }}">
    @yield('style')
</head>
<body>
    <div class="gmo-wrapper">
        <section id="header">
        @include('partials.navbar')
        </section>
        @yield('body')

        <section>
        @include('partials.footer')
        </section>
    </div>
    
    <!-- SCRIPT GOES HERE -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script>
        $('.search-nav').on('click', function() {
            if ($('.gmo-search').hasClass('search-active')) {
                $('.gmo-search').removeClass('search-active')
            } else {
                $('.gmo-search').addClass('search-active');
            }
        })
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();
            // CHANGE THIS TO ADD CLASS TO ACTIVE
            if (scroll >= 500) {
                $('#gmo-hidden-navbar').addClass('activated-nav');
            } else {
                $('#gmo-hidden-navbar').removeClass('activated-nav')                
            }
        });
    </script>
    @yield('script')
</body>
</html>