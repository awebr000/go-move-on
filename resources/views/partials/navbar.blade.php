<nav id="gmo-main-navbar">
  <div class="row">
    <!-- NAV FOR MOBILE VIEW -->
    <div style="text-align: center; padding-right: 0" class="d-block d-sm-none col-3">
      <div id="navbar-menu-wrapper">
        <ul id="gmo-navbar-menu">
          <li class="gmo-nav-item">
            <span onclick="test()" class="gmo-link-default" style="font-size: 40px;"><i class="fa fa-bars"></i></span>
          </li>
        </ul>
      </div>
    </div>
    <div class="d-block d-sm-none col-9">
      <a href="/">  
        <img style="width: 100%" src="{{asset('images/navbar-bg.jpeg')}}" alt="">    
      </a>
    </div>
    <!-- END HERE -->


    <!-- NAV IN FULL PAGE -->
    <div class="d-none d-md-block d-lg-block d-xl-block col-md-3">
      <a href="/">
        <img style="width: 100%" src="{{asset('images/navbar-bg.jpeg')}}" alt="">    
      </a>  
    </div>
    <div class="d-none d-md-block d-lg-block d-xl-block col-md-9" style="padding-top: 55px">
      <div id="navbar-menu-wrapper">
        <ul id="gmo-navbar-menu">
          <li class="gmo-nav-item">
           <a href="/hotnews" class="gmo-link-default gmo-white">HOTNEWS</a>
          </li>
          <li class="gmo-nav-item">
            <a href="/highlight" class="gmo-link-default gmo-white">HIGHLIGHT</a>
          </li>
          <li class="has-sub gmo-nav-item" style="position: absolute; bottom: 5px">
            <a href="#" id="event-dropdown" class="gmo-link-default gmo-white">EVENTS</a>
            <ul class="gmo-sub-menu" id="event-sub">
              <li><a href="#" class="gmo-link-default">GIGS</a></li>
              <li><a href="#" class="gmo-link-default">PENSI</a></li>
              <li><a href="#" class="gmo-link-default">PAMERAN</a></li>
            </ul>
          </li>
          <li class="gmo-nav-item" style="float: right; padding-top: 0">
            <input class="gmo-search" type="text" placeholder="search">  
            <i class="search-nav gmo-link-default gmo-white">
              <i class="fa fa-search"></i>
            </i>
          </li>
        </ul>
      </div>
    </div>
    <!-- END HERE -->
  </div>
</nav>


<!-- HIDDEN NAVBAR IS HERE -->


<nav id="gmo-hidden-navbar">
  <div class="row">
    <div class="col-md-2">
      <a href="/">  
        <img style="width: 100%" src="{{asset('images/navbar-bg.jpeg')}}" alt="">    
      </a>
    </div>
    <div class="col-md-10" style="padding-top: 15px">
      <div id="navbar-menu-wrapper">
        <ul id="gmo-navbar-menu">
          <li class="gmo-nav-item">
           <a href="" class="gmo-link-default gmo-white">HOTNEWS</a>
          </li>
          <li class="gmo-nav-item">
            <a href="" class="gmo-link-default gmo-white">HIGHLIGHT</a>
          </li>
          <li class="has-sub gmo-nav-item" style="position: absolute; bottom: 11px">
            <a href="" id="event-dropdown" class="gmo-link-default gmo-white">EVENTS</a>
            <ul class="gmo-sub-menu" id="event-sub">
              <li><a href="#" class="gmo-link-default">GIGS</a></li>
              <li><a href="#" class="gmo-link-default">PENSI</a></li>
              <li><a href="#" class="gmo-link-default">PAMERAN</a></li>  
            </ul>
          </li>
          <li class="gmo-nav-item" style="float: right; padding-top: 0">
            <input class="gmo-search" type="text" placeholder="search">  
            <i class="search-nav gmo-link-default gmo-white">
              <i class="fa fa-search"></i>
            </i>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>


<!-- SIDEBAR -->
<div id="mySidenav" class="sidenav">
    <div class="container" style="background-color: #2874f0; padding-top: 10px;">
        <span class="sidenav-heading">Home</span>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    </div>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
    <a href="http://clashhacks.in/">Link</a>
</div>

<script>
  function test() {
    document.getElementById("mySidenav").style.width = "70%";
  }
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }
</script>