<style>
    #footer {
        border-top: 5px solid white;
        border-bottom: 5px solid white;
        margin-top: 20px;
        margin-bottom: 20px;
        background-color: #0e0e0e;
        padding: 10px 15px;
    }
    .gmo-footer-menu {
        padding: 5px 0;
    }
    .gmo-footer-menu span {
        color: white;
        padding: 0 15px;
    }
    .gmo-footer-menu span a{
        color: white;
    }
    .gmo-footer-menu span:first-child {
        padding-left: 10px;
    }
    .gmo-footer-socials span {
        color: white;
        font-size: 24px;
        padding: 0 15px;
        float: right;
    }
    .gmo-footer-socials span a{
        color: white;
    }
    .gmo-footer-socials span:first-child {
        padding-right: 0;
    }
    .gmo-footer-socials {
        padding-right: 10px;
        padding-left: 0;
        padding-top: 0;
        padding-bottom: 0;
    }
    .gmo-twitter:hover {
      color : #28AAE1
    }
    .gmo-instagram:hover {
      color: #CB39A6
    }
    .gmo-youtube:hover {
      color: #FF0000
    }
</style>

<div id="footer">
    <div class="row">
        <div class="col-md-9 gmo-footer-menu">
            <span><a class="gmo-link-default" href="">HOME</a></span>
            <span><a class="gmo-link-default" href="">CONTACT</a></span>
            <span><a class="gmo-link-default" href="">ABOUT</a></span>
        </div>
        <div class="col-md-3 gmo-footer-socials">
            <span><a class="gmo-link-default gmo-twitter" href="https://twitter.com/GMO_Event"><i class="fa fa-twitter"></i></a></span>
            <span><a class="gmo-link-default gmo-instagram" href="https://instagram.com/gmo_event/"><i class="fa fa-instagram"></i></a></span>
            <span><a class="gmo-link-default gmo-youtube" href="https://www.youtube.com/channel/UCZVo-pETCJ4mzASw3GLcZpw"><i class="fa fa-youtube"></i></a></span>
        </div>
    </div>
</div>
