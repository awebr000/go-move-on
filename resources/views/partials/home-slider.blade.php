@php
  if (count($sliders) > 0) {
    $activeSlide = $sliders[0];
    unset($sliders[0]);
  }
@endphp
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    @foreach($sliders as $key => $slide)
    <li data-target="#demo" data-slide-to="{{$key}}"></li>
    @endforeach
  </ul>

  <!-- The slideshow -->
  @if(isset($activeSlide))
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="slider" src="{{$activeSlide->url}}" alt="Los Angeles">
    </div>
    @foreach($sliders as $slide)
    <div class="carousel-item">
      <img class="slider" src="{{$slide->url}}" alt="Los Angeles">
    </div>
    @endforeach
  </div>
  @endif

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>