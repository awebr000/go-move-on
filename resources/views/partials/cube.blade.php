<div id="mainDiv">
  <div id="boxDiv">
    <div id="F">
        <img style="width:100%" src="{{asset('images/cube_images/indiecloth-main.jpeg')}}" alt="">
    </div>
    <div id="BC">
        <img style="width:100%" src="{{asset('images/cube_images/unbeaten.jpeg')}}" alt="">        
    </div>
    <div id="L">
        <img style="width:100%" src="{{asset('images/cube_images/indiego-main.jpeg')}}" alt="">
    </div>
    <div id="R">
        <img style="width:100%" src="{{asset('images/cube_images/gianthard.jpeg')}}" alt="">
    </div>
  </div>
    <div id="button">
        <i class="fa fa-caret-left cube-icon" onclick=turnRight()></i>
        <i class="fa fa-caret-right cube-icon" style="float: right"onclick=turnLeft()></i>
    </div>
</div>


<script>
var cubex = 0,    // initial rotation
cubey = 0,
cubez = 0;
function rotate(variableName, degrees) {
    window[variableName] = window[variableName] + degrees;
    rotCube(cubex, cubey, cubez);
}
function rotCube(degx, degy, degz){
    segs = "rotateX("+degx+"deg) rotateY("+degy+"deg) rotateZ("+degz+"deg) translateX(0) translateY(0) translateZ(0)";
    $('#boxDiv').css({"transform":segs});
}
function turnRight() {
    rotate("cubey", 90);
}
function turnLeft() {
    rotate("cubey", -90);
}
function flipCube() {
    rotate("cubez", -180);
}

</script>