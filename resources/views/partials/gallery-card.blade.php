<div class="row">
    @foreach($galleries as $gal)
    <div class="gmo-gallery-item col-4 col-md-4">
        <img style="width: 100%" src="{{$gal->url}}" alt="">
    </div>
    @endforeach
</div>