<!-- 
    PARAMETER TO USE THIS DIRECTIVES
    $content = Content Model
    $name = string
    $type = string
 -->

<div class="row gmo-article-card">
    <div class="d-none d-md-block d-lg-block d-xl-block col-md-6 gmo-no-mp">
        <a href="{{route('content.'. $name .'.show', ['metaTitle' => $content->meta_title])}}">
            <img src="{{$content->bg_thumb_image ?: asset('images/1.jpg')}}" class="gmo-img-thumbnail" alt="">
        </a>
    </div>
    <div class="d-block d-sm-none col-2 gmo-no-mp">
        <img src="{{$content->sm_thumb_image ?: asset('images/1.jpg')}}" class="gmo-img-thumbnail" alt="">    
    </div>
    <div class="gmo-desc-wrapper d-block d-sm-none col-9">
        <div class="gmo-article-title">
            <p style="margin-bottom: 3px">
                <a class="gmo-link-default" href="{{route('content.'. $name .'.show', ['metaTitle' => $content->meta_title])}}">
                    {{$content->title}}
                </a>
            </p>
        </div>           
    </div>
    <div class="d-none d-md-block d-lg-block d-xl-block col-md-6">
        <div class="gmo-article-title">
            <h5>
            <a class="gmo-link-default" href="{{route('content.'. $name .'.show', ['metaTitle' => $content->meta_title])}}">
                    {{$content->title}}
                </a>
            </h5>
        </div>
        <div class="gmo-article-info">
            <span><i class="fa fa-clock-o"></i> {{$content->published_at->format('d F Y')}}</span>
            <span><i class="fa fa-eye"></i> {{$content->views}}</span>
            <span><i class="fa fa-comment-o"></i> 1</span>
        </div>
        <p>       
            {{substr($content->description, 0, 200) . '...'}}
        </p>
    </div>
</div>