@extends('layouts.app')
@section('title', ' - Home')

<!-- STYLE GOES HERE -->
@section('style')
    <style>
        
    </style>
@endsection

@section('body')
    <section id="gmo-home-wrapper">
        <div id="gmo-home-carousel">
            @include('partials.home-slider', ['sliders' => $sliders])
        </div>
        <div id="gmo-home-content" class="row">
            <div class="col-md-7">
                <!-- HOT NEWS -->
                <div id="hot-news">
                    <div class="title-block">
                        <h3 class="text">
                            <span>HOT NEWS</span>
                            <span class="view-all"><a href="/hotnews" class="gmo-link-default">VIEW ALL</a></span>
                        </h3>
                    </div>
                    <div class="gmo-article-thumbnails">
                        @if(!empty($hot_news))
                        @include('partials.article-card', ['content' => $hot_news, 'type' => 'hotnews', 'name' => 'hotnews'])
                        @endif
                    </div>
                </div>
                <!-- HIGHLIGHT -->
                <div id="hot-news">
                    <div class="title-block">
                        <h3 class="text">
                            <span>HIGHLIGHT</span>
                            <span class="view-all"><a href="/highlight" class="gmo-link-default">VIEW ALL</a></span>
                        </h3>
                    </div>
                    <div class="gmo-article-thumbnails">
                        @if(!empty($highlight))                        
                        @include('partials.article-card', ['content' => $highlight, 'type' => 'highlight', 'name' => 'highlight'])
                        @endif
                    </div>
                </div>
                <div id="galleries">
                    <div class="title-block">
                        <h3 class="text">
                            <span>GALLERIES</span>
                            <span class="view-all"><a href="/galleries" class="gmo-link-default">VIEW ALL</a></span>
                        </h3>
                    </div>
                    <div class="gmo-article-thumbnails">
                        @include('partials.gallery-card')
                    </div>
                </div>
            </div>
            <div class="d-none d-md-block d-lg-block d-xl-block col-md-5">
                <div>
                    @include('partials.cube')
                </div>
                <!-- <div>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe harum excepturi, nam iusto rerum adipisci ea commodi porro ut obcaecati tenetur neque dolor quae at officiis, quis reiciendis assumenda fugiat.
                </div> -->
            </div>
        </div>
    </section>
    
@endsection

@section('script')

@endsection
