@extends('layouts.app')
@section('title', 'galleries')



@section('body')
<section id="gmo-home-wrapper" style="padding: 15px">
    @foreach($galleries->chunk(4) as $gallery)
        <div class="row">
            @foreach($gallery as $img)
                <div class="col-md-3">
                    <img src="{{$img->url}}" style="width: 100%" alt="">
                </div>
            @endforeach
        </div>
    @endforeach
</section>
@endsection

@section('script')

@endsection
