@extends('layouts.app')
@section('title', $title)

@section('style')
<link rel="stylesheet" href="{{asset('css/content.css')}}">
@endsection

@php
if (count($contents) > 0) {
    $mainContent = $contents[0];
    unset($contents[0]);
}
@endphp

@section('body')
<section id="gmo-home-wrapper" style="padding: 15px">
    @if(!empty($mainContent))
    <a href="{{route('content.hotnews.show', ['metaTitle' => $mainContent->meta_title])}}">
        <div id="gmo-banner-top" style='background-image: url("{{$mainContent->bg_thumb_image}}")'>
            <div class="banner-desc">
                <h3 class="banner-header gmo-white">{{$mainContent->title}}</h3>
                <p class="banner-text gmo-white">{{$mainContent->description}}</p>
            </div>
        </div>
    </a>
    @endif
    <div id="gmo-home-content" class="row">
        <div class="col-md-7">
            <div id="hot-news">
                @foreach($contents as $content)
                <div class="gmo-article-thumbnails">
                    @if(!empty($content))
                    @include('partials.article-card', ['content' => $content, 'type' => 'hotnews', 'name' => 'hotnews'])
                    @endif
                </div>
                @endforeach
            </div>
        </div>
        <div class="d-none d-md-block d-lg-block d-xl-block col-md-5">
            <div>
                @include('partials.cube')
            </div>
            <!-- <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe harum excepturi, nam iusto rerum adipisci ea commodi porro ut obcaecati tenetur neque dolor quae at officiis, quis reiciendis assumenda fugiat.
            </div> -->
        </div>
    </div>
</section>
@endsection

@section('script')

@endsection
