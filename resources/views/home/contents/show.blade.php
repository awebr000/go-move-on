@extends('layouts.app')
@section('title', $title)

@section('style')
<link rel="stylesheet" href="{{asset('css/content.css')}}">
@endsection

@section('body')
{{$showedContent->plusView()}}
<section id="gmo-home-wrapper" style="padding: 15px">
    <div id="gmo-home-content" class="row">
        @if(!empty($showedContent))
        <div class="col-md-7" style="margin-top: 5px">
            <img src="{{$showedContent->bg_thumb_image}}" style="width: 100%" alt="">
            <div style="margin-top: 15px">
                <div class="gmo-content-title">
                    <h3>{{$showedContent->title}}</h3>  
                </div>
                <div>
                    <span class="gmo-content-info">{{$showedContent->published_at->diffForHumans()}} |</span>
                    <span class="gmo-content-info">{{$showedContent->views}} views |</span>
                </div>
                <div class="gmo-content-text">
                    {!! $showedContent->content !!}
                </div>
            </div>
        </div>
        @else
        <div class="col-md-7" style="margin-top: 5px">
            <p>There's no content here</p>
        </div>
        @endif
        <div class="gmo-cube-area d-none d-md-block d-lg-block d-xl-block col-md-5">
            <div>
                @include('partials.cube')
            </div>
            <!-- <div>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe harum excepturi, nam iusto rerum adipisci ea commodi porro ut obcaecati tenetur neque dolor quae at officiis, quis reiciendis assumenda fugiat.
            </div> -->
        </div>
    </div>
</section>
@endsection

@section('script')

@endsection
