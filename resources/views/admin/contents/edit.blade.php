@extends('layouts.base-admin')
@section('title', 'Admin Panel')

@section('body')
<div id="gmo-admin-home" style="min-height: 700px;">
    <div id="gmo-admin-content" style="margin-bottom: 30px;">
        <h3 style="border-bottom: 3px solid black; padding-bottom: 5px">CREATE NEW CONTENT</h3>
        <div class="form-group">
            <select class="form-control form-control-sm" id="tag-select">
                @foreach($tags as $tag)
                    <option value="{{$tag->id}}" {{$tag->id == $content->tag_id ? 'selected' : ''}}>{{ucfirst(str_replace('_', ' ', $tag->name))}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$content->title}}" required id="title" placeholder="Enter Title*">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" rows="3">{{$content->description}}</textarea>
        </div>
        <div id="summernote">
            {!! $content->content !!}
        </div>

        <br>
        <div class="form-group">
            <label for="bg-thumbnail">Add Big Thumbnail Image*</label>
            <input type="file" name="file" id="bg-thumbnail" class="form-control-file">
            <input type="hidden" value="{{$content->bg_thumb_image}}" id="bg-thumb-link">
            <div class="row">
                <div class="col-md-4">
                    <img style="width: 100%" src="{{$content->bg_thumb_image}}" id="bg-thumb-preview" alt="">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="sm-thumbnail">Add Small Thumbnail Image (for mobile)*</label>
            <input type="file" name="file" id="sm-thumbnail" class="form-control-file">
            <input type="hidden" value="{{$content->sm_thumb_image}}" id="sm-thumb-link">
            <div class="row">
                <div class="col-md-2">
                    <img style="width: 100%" src="{{$content->sm_thumb_image}}" id="sm-thumb-preview" alt="">
                </div>
            </div>
        </div>
    </div>

    <button id="save" class="btn btn-success">
        save
    </button>
    or
    <button id="save_publish" class="btn btn-success">
        save and publish
    </button>
</div>
@endsection

@section('script')
<script>
    $('#summernote').summernote({
        focus: true,        
        tabsize: 2,
        height: 300,
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            }
        }
    });

    $('#bg-thumbnail').on('change', function() {
        var file = $(this)[0].files[0];
        uploadFile($(this)[0].files[0], 'bg_content_thumbnail').then(function(url) {
            $('#bg-thumb-link').val(url)
            $('#bg-thumb-preview').attr('src', url)
        });
    });

    $('#sm-thumbnail').on('change', function() {
        var file = $(this)[0].files[0];
        uploadFile($(this)[0].files[0], 'sm_content_thumbnail').then(function(url) {
            $('#sm-thumb-link').val(url)
            $('#sm-thumb-preview').attr('src', url)
        });
    });

    /** UPLOAD FILE FUNCTION FOR ALL */
    function uploadFile(file, type) {
        var data = new FormData();
        data.append('_token', "{{csrf_token()}}")
        data.append("image", file);
        data.append('type', type)
        return Promise.resolve(
            $.ajax({
                url: "{{route('admin.UploadImage')}}",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function(url) {
                    return url;
                },
                error: function(data) {
                    console.log(data);
                }
            })
        )
    }

    function uploadImage(img) {
        var data = new FormData();
            data.append('_token', "{{csrf_token()}}")
            data.append("image", img);
            data.append('type', 'content_image')
            $.ajax({
                url: "{{route('admin.UploadImage')}}",
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function(url) {
                    var image = $('<img>').attr('src', url);
                    $('#summernote').summernote("insertNode", image[0]);
                },
                error: function(data) {
                    console.log(data);
                }
            });
    }
    
    $('#save').on('click', function() {
        var markupStr = $('#summernote').summernote('code');
        var tag_id = $('#tag-select').val();
        var data = {
            _token: "{{csrf_token()}}",
            title: $('#title').val(),
            description: $('#description').val(),
            tag_id: tag_id,
            sm_thumb_image: $('#sm-thumb-link').val(),
            bg_thumb_image: $('#bg-thumb-link').val(),
            content: markupStr
        }
        $.ajax({
            url: "{{route('admin.contents.update', ['content' => $content->id])}}",
            data: data,
            type: "put",
            success: function(response) {
                alert("CONTENT SUDAH DI SIMPAN DI DRAFT BOSQUE")
                location.reload()
            },
            error: function(data) {
                alert("BILANG KE DEVELOPER ADA ERROR!")
                location.reload()
            }
        });
    })

    $('#save_publish').on('click', function() {
        var markupStr = $('#summernote').summernote('code');
        var tag_id = $('#tag-select').val();
        var data = {
            _token: "{{csrf_token()}}",
            title: $('#title').val(),
            description: $('#description').val(),
            tag_id: tag_id,
            content: markupStr,
            sm_thumb_image: $('#sm-thumb-link').val(),
            bg_thumb_image: $('#bg-thumb-link').val(),
            publish: true
        }
        $.ajax({
            url: "{{route('admin.contents.update', ['content' => $content->id])}}",
            data: data,
            type: "put",
            success: function(response) {
                alert("CONTENT SUDAH DI PUBLISH BOSQUE")
                location.reload()
            },
            error: function(data) {
                alert("BILANG KE DEVELOPER ADA ERROR!")
                location.reload()
            }
        });
    })

</script>
@endsection