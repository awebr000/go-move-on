@extends('layouts.base-admin')
@section('title', 'Admin Panel')

@section('body')
<div id="gmo-admin-home" style="min-height: 700px;">
    <div id="gmo-admin-content" style="margin-bottom: 30px;">
        <h3 style="border-bottom: 3px solid black; padding-bottom: 5px">CONTENT LIST
            <span style="float: right; font-size: 14px; margin-top: 10px"><a class="gmo-link-inverse" href="{{route('admin.contents.create')}}">Create New</a></span>
        </h3>
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Category</th>
                <th scope="col">Views</th>
                <th scope="col">Published At</th>
                <th scope="col">Created At</th>
                <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contents as $content)
                <tr>
                    <th scope="row">{{$content->id}}</th>
                    <td>{{$content->title}}</td>
                    <td>{{$content->description}}</td>
                    <td>{{ucfirst(str_replace('_', ' ', $content->tag->name))}}</td>
                    <td>{{$content->views}}</td>
                    <td>{{$content->published_at ? $content->published_at->format('d-m-Y h:i:s A') : 'not published yet'}}</td>
                    <td>{{$content->created_at->format('d-m-Y')}}</td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Action
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{route('admin.contents.show', ['metaTitle' => $content->meta_title])}}">Preview</a>
                                <a class="dropdown-item" href="{{route('admin.contents.edit', ['id' => $content->id])}}">Edit</a>
                                <a class="content-publish dropdown-item" href="#" data-id="{{$content->id}}" data-published="{{$content->published_at ? 'true' : 'false'}}">{{$content->published_at ? 'Unpublish' : 'Publish'}}</a>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$contents->links()}}
    </div>
</div>
@endsection

@section('script')
<script>
    $('.content-publish').on('click', function() {   
        var published = $(this).data('published')    
        var contentId = $(this).data('id')
        var data = {
            _token: "{{csrf_token()}}"
        }
        /** SET DATA PUBLISHED OR UNPUBLISH */
        if (published) {
            data.unpublish = true;           
        } else {
            data.publish = true;
        }

        $.ajax({
            url: "{{route('admin.contents.update', ['content' => ':id'])}}".replace(":id", contentId),
            data: data,
            type: "put",
            success: function(response) {
                alert("CONTENT SUDAH DI UPDATE BOSQUE")
                location.reload()
            },
            error: function(data) {
                alert("ADA ERROR BOSQUE")
                location.reload()                
            }
        });
    })
</script>
@endsection