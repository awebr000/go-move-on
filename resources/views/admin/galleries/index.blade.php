@extends('layouts.base-admin')
@section('title', 'Admin Panel')

@section('style')
<style>
    .checked-images {
        padding: 15px;
        border: 3px solid #007bff;
        border-radius: 2px;
    }
    .going-active-img {
        padding: 15px;
        border: 3px solid #007bff;
        border-radius: 2px;
    }
    .deactive-btn {
        display: none;
        margin: 15px;
    }
    .active-btn {
        display: none;
        margin: 15px;
    }
</style>
@endsection

@section('body')
<div id="gmo-admin-home" style="min-height: 700px;">
    <div id="gmo-admin-content" style="margin-bottom: 30px;">
        <h3 style="border-bottom: 3px solid black; padding-bottom: 15px">GALLERY
            <span style="float: right; font-size: 14px;">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    ADD NEW IMAGES
                </button>
            </span>
        </h3>
        <div>
            <p>GALLERIES</p>
            <div>
                @foreach($galleries->chunk(3) as $gallery)
                <div class="row">
                    @foreach($gallery as $img)
                        <div class="col-md-4 inactive-images" data-image-id="{{$img->id}}" style="padding: 15px;">
                            <img src="{{$img->url}}" style="width: 100%" alt="">
                        </div>
                    @endforeach
                </div>
                @endforeach
            </div>
        </div>

        <!-- MODAL -->
        @include('admin.galleries.partials.add-new-image-modal')
    </div>
</div>
@endsection

@section('script')
<script>
    var token = "{{ csrf_token() }}"
    $('.active-images').on('click', function() {
        $(this).toggleClass('checked-images');
        var imgId = $(this).data('image-id');
        $(this).hasClass('checked-images')
        var checkedImages = $('.checked-images');
        if (checkedImages.length > 0) {
            $('.deactive-btn').show();
        } else {
            $('.deactive-btn').hide();            
        }
    })
    
    /** CHANGE THIS TO UPDATE THE IMAGES SLIDER */
    $('.deactive-btn').on('click', function() {
        var checkedImages = $('.checked-images'); 
        var ids = []       
        checkedImages.each(function() {
            ids.push($(this).data('image-id'))
        })

        var data = {
            _token: token,
            ids: ids
        }

        $.ajax({
            url: "{{route('admin.slider-images.massUpdate')}}",
            data: data,
            type: "put",
            success: function(response) {
                alert("GAMBAR SUDAH TIDAK DIJADIKAN SLIDE")
                location.reload()
            },
            error: function(data) {
                alert("BILANG KE DEVELOPER ADA ERROR!")
            }
        });
    })

    $('.inactive-images').on('click', function() {
        $(this).toggleClass('going-active-img');
        var imgId = $(this).data('image-id');
        $(this).hasClass('going-active-img')
        var checkedImages = $('.going-active-img');
        if (checkedImages.length > 0) {
            $('.active-btn').show();
        } else {
            $('.active-btn').hide();            
        }
    })

    $('.active-btn').on('click', function() {
        var checkedImages = $('.going-active-img'); 
        var ids = []
        checkedImages.each(function() {
            ids.push($(this).data('image-id'))
            console.log(ids)
        })
        var data = {
            _token: token,
            ids: ids,
            activate: true
        }
        
        $.ajax({
            url: "{{route('admin.slider-images.massUpdate')}}",
            data: data,
            type: "put",
            success: function(response) {
                alert("GAMBAR SUDAH DIJADIKAN SLIDE")
                location.reload()
            },
            error: function(data) {
                alert("BILANG KE DEVELOPER ADA ERROR!")
            }
        });
    })
</script>
@endsection
