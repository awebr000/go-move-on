@extends('layouts.base-admin')
@section('title', 'Admin Panel')

@section('body')
<div id="gmo-admin-home" style="min-height: 700px;">
    <div id="gmo-admin-content" style="margin-bottom: 30px;">
        <h3 style="border-bottom: 3px solid black; padding-bottom: 5px">CONTENT DASHBOARD</h3>
        <div style="margin-top: 20px" class="row">
            @foreach($data as $key => $d)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body" style="padding: 0">
                        <div style="padding: 1.25rem; background-color: #3AD08C">
                            <h4 class="card-title">{{ucfirst(str_replace('_', ' ',  $key))}}</h4>
                        </div>
                        <div style="padding: 5px 10px; background-color: #F6F6F6">
                            <p class="card-text">PUBLISHED: <span>{{$d['published']}}</span></p>
                            <p class="card-text">UNPUBLISHED: <span>{{$d['unpublished']}}</span></p>
                            <p class="card-text">TOTAL: <span>{{$d['total']}}</span></p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <!-- GALER I -->

    <div id="gmo-gallery-content">
        <h3 style="border-bottom: 3px solid black; padding-bottom: 5px">GALLERY DASHBOARD</h3>
        <div style="margin-top: 20px" class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body" style="padding: 0">
                        <div style="padding: 1.25rem; background-color: #3AD08C">
                            <h4 class="card-title">VIDEO</h4>
                        </div>
                        <div style="padding: 5px 10px; background-color: #F6F6F6">
                            <p class="card-text">TOTAL: <span>13</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body" style="padding: 0">
                        <div style="padding: 1.25rem; background-color: #3AD08C">
                            <h4 class="card-title">IMAGES</h4>
                        </div>
                        <div style="padding: 5px 10px; background-color: #F6F6F6">
                            <p class="card-text">TOTAL: <span>13</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
